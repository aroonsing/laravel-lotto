<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('backend.dashboard.index');
})->middleware('auth');

Route::match(['get', 'post'], 'login', ['as' => 'login', 'uses' => 'Auth\LoginController@signin']);

// Route::match(['get', 'post'], '/signin   ', 'Auth\LoginController@signin');

Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/auth/settings', 'Auth\LoginController@settings');
    Route::get('/auth/check-pwd', 'Auth\LoginController@chkPassword');
    Route::match(['get', 'post'], '/auth/update-password', 'Auth\LoginController@updatePassword');
    Route::match(['get', 'post'], '/signout', 'Auth\LoginController@Signout');
});

Route::group(['middleware' => 'auth', 'prefix' => 'backend'], function () {
    Route::get('/dashboard', 'Backend\DashboardController@Index');


    Route::resource('/admins', 'Backend\AdminsController');

    Route::get('/users/create/{id}', 'Backend\UsersController@create')->name('users.create');
    Route::match(['get', 'post'], '/users/store/{id}', 'Backend\UsersController@store')->name('users.store');
    Route::match(['get', 'post'], '/users/data/{id}', 'Backend\UsersController@indexData');
    Route::match(['get', 'post'], '/users/details-data/{id}', 'Backend\UsersController@indexDataDetail');

    Route::resource('/users', 'Backend\UsersController')->except([
        'create', 'store'
    ]);;
});
