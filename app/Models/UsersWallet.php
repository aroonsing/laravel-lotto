<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersWallet extends Model
{

    protected $table = 'users_wallet';
    public $timestamps = false;
}
