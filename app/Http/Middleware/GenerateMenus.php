<?php

namespace App\Http\Middleware;

use Closure;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Menu::make('adminNavBar', function ($menu) {
            $menu->add('<i class="icon-home4"></i> <span>Dashboard</span>', ['url' => 'backend/dashboard']);
            // $menu->add('<i class="icon-calendar2"></i><span>ระบบจัดการวันทำการ</span>', ['url' => 'backend/working-days']);
            $menu->add('<i class="icon-user-tie"></i><span>ระบบจัดการ Admin</span>', ['url' => 'backend/admins']);
            $menu->add('<i class="icon-users4"></i><span>ระบบจัดการสมาชิก</span>', ['url' => 'backend/users']);
            // $menu->add('<i class="icon-truck"></i><span>ระบบจัดการผู้ส่ง</span>', ['url' => 'backend/senders']);
            // $menu->add('<i class="icon-dropbox"></i><span>ระบบจัดการสินค้า</span>', ['url' => 'backend/products']);

            // $menu->add('<i class="icon-clippy"></i> <span>จัดการใบสั่งซื้อ</span>', '#')->id('1');
            // $menu->add('สร้างใบสั่งซื้อ', ['url' => 'backend/orders/create', 'parent' => 1]);
            // $menu->add('รายละเอียดใบสั่งซื้อ', ['url' => 'backend/orders', 'parent' => 1]);

            // $menu->add('<i class="icon-printer"></i> <span>พิมพ์</span>', '#')->id('2');
            // $menu->add('ใบสั่งซื้อ', ['url' => 'backend/printorders', 'parent' => 2]);
            // $menu->add('ใบเคลม', ['url' => 'backend/printclaimorders', 'parent' => 2]);

            // $menu->add('<i class="icon-clippy"></i> <span>รายงาน</span>', '#')->id('3');
            // $menu->add('รายงานใบสั่งซื้อ', ['url' => 'backend/reportorders', 'parent' => 3]);
            // $menu->add('รายงานใบเคลม', ['url' => 'backend/report-claimorders', 'parent' => 3]);
        });

        return $next($request);
    }
}
