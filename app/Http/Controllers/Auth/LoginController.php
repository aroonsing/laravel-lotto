<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class LoginController extends Controller
{
    public function signin(Request $request)
    {
        // echo session('adminSession');
        if ($request->isMethod('post')) {
            $data = $request->input();

            //was any of those correct ?
            if (Auth::attempt(['username' => $data['username'], 'password' => $data['password']])) {
                //send them where they are going
                return redirect('/backend/dashboard');
            } else {
                return redirect('/login')->with('flash_message_error', 'Invalid Username or Password');
            }
        }
        return view('auth.login');
    }


    public function settings()
    {
        return view('auth.settings');
    }

    public function chkPassword(Request $request)
    {
        $data = $request->all();
        $current_password = $data['current_password'];
        $check_password = User::first();
        if (Hash::check($current_password, $check_password->password)) {
            echo 'true';
            die;
        } else {
            echo 'false';
            die;
        }
    }

    public function updatePassword(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            // echo "<pre>"; print_r($data); die;
            $check_password = User::where(['username' => Auth::user()->username])->first();
            $current_password = $data['current_password'];
            // dd($current_password);
            if (Hash::check($current_password, $check_password->password)) {
                $password = bcrypt($data['new_password']);
                User::where('id', '1')->update(['password' => $password]);
                return redirect('/auth/settings')->with('flash_message_success', 'Password updated Successfully!');
            } else {
                return redirect('/auth/settings')->with('flash_message_error', 'Incorrect Current Password!');
            }
        }
    }

    public function Signout()
    {
        session()->flush();
        Auth::logout();
        return redirect('/login')->with('flash_message_success', 'Logged out Successfully');
    }
}
