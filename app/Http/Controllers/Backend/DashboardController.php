<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        /*if(session('adminSession')){
            //Perform all dashboard tasks.
        }else{
            return redirect('/admin')->with('flash_message_error','Please login to access');
        }*/
        return view('backend.dashboard.index');
    }
}
