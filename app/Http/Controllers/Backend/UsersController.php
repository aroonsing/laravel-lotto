<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\UsersWallet;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use DataTables;
use Illuminate\Support\Facades\Redirect;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model_has_roles = DB::table('model_has_roles')
            ->select('role_id')
            ->where('model_has_roles.model_id', Auth::user()->id)->first();

        $after_role_id = $model_has_roles->role_id + 1;

        $role = DB::table('roles')
            ->select('name')
            ->where('roles.id', $after_role_id)->first();

        $id = 0;
        if ($id == 0) {
            return view('backend.users.index', compact('role', 'id'))->with($id);
        } else {
            return Redirect::to('/backend/users/' . $id)->with(['role' => $role, 'id' => $id]);
        }
    }


    public function indexData($id)
    {
        $model_has_roles = DB::table('model_has_roles')->select('role_id');
        if ($id != 0) {
            $model_has_roles->where('model_has_roles.model_id', $id);
        } else {
            $model_has_roles->where('model_has_roles.model_id', Auth::user()->id);
        }

        $model_has_roles = $model_has_roles->first();

        $after_role_id = $model_has_roles->role_id + 1;

        $data = User::select(
            'users.id',
            'users.parent_id',
            'users.name',
            'users.username',
            'users.mobile',
            'users.locked',
            'roles.name AS roles_name',
            'users_wallet.credit',
            'users_wallet.credit_remain',
            'users_wallet.account_remain',


        )
            ->join('users_wallet', 'users.id', '=', 'users_wallet.users_id')
            ->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')

            ->where('role_id', '=', $after_role_id)
            ->whereNull('deleted_at');

        if ($id != 0) {
            $data->where('parent_id', '=', $id);
        }

        $role_after = DB::table('roles')->select('name')->where('id', '=', $after_role_id)->first();

        if ($role_after->name != 'member') {
            return Datatables::of($data)
                ->addColumn('details_url', function ($data) {
                    return url('backend/users/' . $data->id);
                })
                ->make();
        } else {
            return Datatables::of($data)->make();
        }

        return Datatables::of($data)
            ->addColumn('details_url', function ($data) {
                return url('backend/users/' . $data->id);
            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id = null)
    {
        $model_has_roles = DB::table('model_has_roles')->select('role_id');
        if ($id != 0) {
            $model_has_roles->where('model_has_roles.model_id', $id);
        } else {
            $model_has_roles->where('model_has_roles.model_id', Auth::user()->id);
        }

        $model_has_roles = $model_has_roles->first();

        $after_role_id = $model_has_roles->role_id + 1;

        $role = DB::table('roles')
            ->select('*')
            ->where('roles.id', $after_role_id)->first();



        if ($role->name == 'owner') {
            $data = null;
        } else {
            $data = DB::table('users')
                ->select(
                    'users.id',
                    'users.username',
                    'users_wallet.credit',
                    'users_wallet.credit_remain',
                    'users_wallet.account_remain',
                )
                ->join('users_wallet', 'users.id', '=', 'users_wallet.users_id')
                ->where('users.id', '=', $id)
                ->whereNull('deleted_at')->first();
        }

        // dd($data);


        return view('backend.users.create', compact('role', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {

        $username = $request['username'];
        $request['credit'] = str_replace(",", "", $request['credit']);
        $usernameOwner = '';
        Auth::user()->getRoleNames()[0];
        if ($id == 0) {
            if (Auth::user()->getRoleNames()[0] != 'admin') {
                $usersOwner = User::select('username', 'credit_remain')
                    ->join('users_wallet', 'users.id', '=', 'users_wallet.users_id')
                    ->where('users.id', '=', Auth::user()->id)
                    ->first();
                $usernameOwner = $usersOwner->username;
            }
        } else {
            $usersOwner = User::select('username', 'credit_remain')
                ->join('users_wallet', 'users.id', '=', 'users_wallet.users_id')
                ->where('users.id', '=', $id)
                ->first();
            $usernameOwner = $usersOwner->username;
        }
        $username = $usernameOwner . '' . $username;

        $request['username'] = $username;

        if (isset($usersOwner->credit_remain)) {
            if($usersOwner->credit_remain < $request['credit']){
                return Redirect::back()->withErrors('ให้เครดิตมากกว่าเครดิตที่ให้ได้. ')->withInput($request->all());
            }

        }


        $this->validate($request, [
            'name' => 'required',
            'username' => 'required|unique:users,username',
            'password' => 'required',
            'credit' => 'required|integer',
        ]);

        if ($id == 0) {
            if (Auth::user()->getRoleNames()[0] == 'admin') {
                $parent_id = 1;
            } else {
                $parent_id = Auth::user()->id;
            }
        } else {
            $parent_id = $id;
        }

        $data = $request->all();
        $data['password'] = Hash::make($data['password']);

        if (isset($data['locked']) && $data['locked'] == 'on') {
            $data['locked'] = 0;
        } else {
            $data['locked'] = 1;
        }

        $model_has_roles = DB::table('model_has_roles')->select('role_id');
        if ($id != 0) {
            $model_has_roles->where('model_has_roles.model_id', $id);
        } else {
            $model_has_roles->where('model_has_roles.model_id', Auth::user()->id);
        }
        $model_has_roles = $model_has_roles->first();
        $after_role_id = $model_has_roles->role_id + 1;



        DB::beginTransaction();
        try {

            $user = new User;
            $user->parent_id    =   $parent_id;
            $user->name     =   $data['name'];
            $user->username     =   $data['username'];
            $user->password     =   Hash::make($data['password']);
            $user->mobile     =   $data['mobile'];
            $user->username     =   $data['username'];
            $user->locked     =   $data['locked'];
            $user->save();

            $users_wallet = new UsersWallet;
            $users_wallet->users_id = $user->id;
            $users_wallet->credit = $data['credit'];
            $users_wallet->credit_remain = $data['credit'];
            $users_wallet->account_remain = 0.00;
            $users_wallet->save();

            $user->assignRole($after_role_id);

            if($parent_id){
                UsersWallet::where(['users_id' => $parent_id])
                ->update([
                    'credit_remain' => DB::raw('credit_remain-'.$data['credit'])
                ]);
            }


            DB::commit();

            if ($id == 0) {
                return redirect()->route('users.index')
                    ->with('success', 'User created successfully');
            } else {
                return redirect()->route('users.show', $id)
                    ->with('success', 'User created successfully');
            }


            // all good
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model_has_roles = DB::table('model_has_roles')
            ->select('role_id')
            ->where('model_has_roles.model_id', $id)->first();


        $role = DB::table('roles')
            ->select('name')
            ->where('roles.id', $model_has_roles->role_id + 1)->first();

        return view('backend.users.index', compact('role', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('backend.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'username' => 'required|unique:users,username,' . $id,
            'password' => 'same:confirm-password'
        ]);


        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = array_except($input, array('password'));
        }


        $user = User::find($id);
        $user->update($input);


        return redirect()->route('users.index')
            ->with('success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
            ->with('success', 'User deleted successfully');
    }
}
