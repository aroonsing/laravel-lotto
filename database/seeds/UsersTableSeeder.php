<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Role::create(['name' => 'admin']);
        Role::create(['name' => 'owner']);
        // Role::create(['name' => 'super']);
        // Role::create(['name' => 'agent']);
        Role::create(['name' => 'member']);

        $useradmin = App\Models\User::create([
            'parent_id' => 0,
            'name' => 'Administrator',
            'username' => 'admin',
            'password' => bcrypt('aa123456'),
        ]);

        $useradmin->assignRole([1]);

    }
}
