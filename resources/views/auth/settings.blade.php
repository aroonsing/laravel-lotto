@extends('layouts.limitless.index')

@section('extra-scripts')
<!-- Theme JS files -->
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/validation/validate.min.js')}}">
</script>
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/selects/bootstrap_multiselect.js')}}">
</script>
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/inputs/touchspin.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/styling/switch.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/styling/switchery.min.js')}}">
</script>
<script type="text/javascript" src="{{ asset('assets_backend/js/plugins/forms/styling/uniform.min.js')}}"></script>

{{-- <script type="text/javascript" src="{{ asset('assets_backend/js/pages/form_validation.js')}}"></script> --}}
<!-- /theme JS files -->
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">Account Settings</h5>
        <div class="heading-elements">
        </div>
    </div>

    <div class="panel-body">
        @if(Session::has('flash_message_error'))
        <script>
            $(function() {
                new PNotify({
                    title: 'Danger notice',
                    text: '{!! session('flash_message_error') !!}',
                    icon: 'icon-blocked',
                    type: 'error'
                });
            });
        </script>
        @endif
        @if(Session::has('flash_message_success'))
        <script>
            $(function() {
            // Success notification
                new PNotify({
                    title: 'Success notice',
                    text: '{!! session('flash_message_success') !!}',
                    icon: 'icon-checkmark3',
                    type: 'success'
                });
            });
        </script>
        @endif


        <form class="form-horizontal form-validate-jquery" method="POST" action="{{ url('/auth/update-password') }}">
            @csrf
            <fieldset class="content-group">
                <legend class="text-bold">Change Password</legend>

                <!-- Password field -->
                <div class="form-group">
                    <label class="control-label col-lg-3">Current Password <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="password" name="current_password" id="current_password" class="form-control"
                            placeholder="Current Password">
                        <span id="chkPwd"></span>
                    </div>
                </div>
                <!-- /password field -->
                <!-- Password field -->
                <div class="form-group">
                    <label class="control-label col-lg-3">New Password <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="password" name="new_password" id="new_password" class="form-control"
                            required="required" placeholder="New Password">
                    </div>
                </div>
                <!-- /password field -->

                <!-- Repeat password -->
                <div class="form-group">
                    <label class="control-label col-lg-3">Confirm Password <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="password" name="confirm_password" class="form-control" required="required"
                            placeholder="Try different password">
                    </div>
                </div>
                <!-- /repeat password -->
                <div class="text-right">
                    <button type="reset" class="btn btn-default" id="reset">Reset <i
                            class="icon-reload-alt position-right"></i></button>
                    <button type="submit" class="btn btn-primary">Submit <i
                            class="icon-arrow-right14 position-right"></i></button>
                </div>
        </form>
    </div>
</div>
<script>
    $(function() {


        $("#current_password").keyup(function(){
            var current_password = $('#current_password').val();
            // console.log(current_password);
            $.ajax({
                type:'get',
                url:'/auth/check-pwd',
                data:{current_password:current_password},
                success:function(resp){
                    // alert(resp);
                    if(resp == 'false'){
                        $('#chkPwd').html('<label id="current_password-error" class="validation-error-label" for="current_password">Current Password is Incorrect.</label>');
                    }else{
                        $('#chkPwd').html('<label id="new_password-error" class="validation-error-label validation-valid-label" for="new_password" style="">Success.</label>');
                    }
                },error:function(){
                    alert("error");

                }
            });
        });

        var validator = $(".form-validate-jquery").validate({
            ignore: 'input[type=hidden]', // ignore hidden fields
            errorClass: 'validation-error-label',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            validClass: "validation-valid-label",
            success: function(label) {
                label.addClass("validation-valid-label").text("Success.")
            },
            rules: {
                new_password: {
                    minlength: 5
                },
                confirm_password: {
                    equalTo: "#new_password"
                }
            },
            messages: {
                custom: {
                    required: "This is a custom error message",
                },
                agree: "Please accept our policy"
            }
        });


        // Reset form
        $('#reset').on('click', function() {
            validator.resetForm();
        });

    });
</script>
@endsection
