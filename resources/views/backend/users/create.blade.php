@extends('layouts.limitless.index')
@section('content')

<div class="row">
    <div class="col-md-4">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if ($data == null)
        {!! Form::model($data, ['method' => 'POST','class'=>'form-horizontal','route' => ['users.store', 0]])
        !!}
        @else
        {!! Form::model($data, ['method' => 'POST','class'=>'form-horizontal','route' => ['users.store', $data->id]])
        !!}
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">สร้าง {{$role->name}}<a class="heading-elements-toggle"><i
                            class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    @if ($data == null)
                    <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
                    @else
                    <a class="btn btn-primary" href="{{ route('users.show',$data->id) }}"> Back</a>
                    @endif


                </div>
            </div>
            <div class="panel-body">
                <fieldset class="content-group">
                    <legend class="text-bold">ข้อมูลทั่วไป</legend>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Username:<span class="text-danger">*</span></label>

                        @if ($role->name != 'owner')
                        <div class="col-xs-3">
                            <input type="text" value="{{$data->username}}" class="form-control" disabled="disabled"
                                value="disabled">
                        </div>
                        @endif
                        @if ($role->name == 'member')
                        <div class="col-lg-6">
                            <input name="username" type="text" class="form-control" placeholder="Enter Username">
                        </div>
                        @else
                        <div class="col-xs-3">
                            <select name="username" class="form-control">

                                <option value="">a-z</option>
                                @php
                                foreach (range('a', 'z') as $char) {
                                echo '<option value="'.$char.'">'.$char.'</option>';
                                }
                                @endphp
                            </select>
                        </div>
                        @endif

                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Name:<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Password:<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input name="password" type="text" class="form-control" placeholder="Enter password" value="aa123456">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Mobile:</label>
                        <div class="col-lg-9">
                            <input name="mobile" type="text" class="form-control" placeholder="Enter mobile number">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">สถานะเปิดใช้งาน:</label>
                        <div class="col-lg-9">
                            <label>
                                <input name='locked' type="checkbox" data-on-color="success" data-off-color="danger"
                                    data-on-text="เปิด" data-off-text="ปิด" class="switch" checked="checked">
                                เปิด/ปิด
                            </label>
                        </div>
                    </div>

                </fieldset>
                <fieldset class="content-group">
                    <legend class="text-bold">ข้อมูลเครดิต</legend>
                    @if ($data != null)
                        <div class="form-group">
                            <label class="col-lg-3 control-label">เครดิตที่ให้ได้:</label>
                            <div class="col-lg-9">
                            <input value="{{number_format($data->credit_remain)}}" type="text" class="form-control" disabled="disabled" value="disabled">
                            <span class="help-block">เครดิสที่ได้รับ: <code>{{number_format($data->credit)}}</code></span>
                            </div>
                        </div>
                    @endif

                    <div class="form-group">
                        <label class="col-lg-3 control-label">ให้เครดิต:</label>
                        <div class="col-lg-9">
                            <input name="credit" value="0" type="text" id="credit" class="form-control" placeholder="Enter Credit">
                            <span class="help-block text-danger">* ให้เครดิสได้ <code>ไม่เกิน</code> เครดิตที่ให้ได้ </span>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit form <i
                                class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </fieldset>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $(function() {
        $(".switch").bootstrapSwitch();
        $('#credit').mask('000,000,000,000,000', {reverse: true});
    });
</script>
@endsection
