@extends('layouts.limitless.index')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">ระบบจัดการสมาชิก <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            {{-- @can('user-create') --}}
            <a class="btn btn-success heading-btn" href="{{ route('users.create',[$id]) }}">สร้าง {{$role->name}}</a>
            {{-- @endcan --}}
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-xxs" id="test-datatable">
                <thead>
                    <tr class="">
                        <th class="col-xs-2">Username</th>
                        <th class="col-xs-1">Locked</th>
                        <th class="col-xs-2">Name</th>
                        <th class="col-xs-1">Mobile</th>
                        <th class="col-xs-1">Role Name</th>
                        <th class="col-xs-1">Credit</th>
                        <th class="col-xs-1">Credit <br> คงเหลือ</th>
                        <th class="col-xs-1">ยอดบัญชี</th>
                        <th class="">Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<script>
    $(function() {
        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var table = $('#test-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('backend/users/data')}}/{{$id}}",
            columns: [

                {"data": null , name :'username'},
                {"data": null ,orderable: false, searchable: false},
                {"data":'name'},
                {"data": 'mobile'},
                {"data": null ,orderable: false, searchable: false},
                {"data": 'credit',render: $.fn.dataTable.render.number( ',', '.', 2 )},
                {"data": 'credit_remain',render: $.fn.dataTable.render.number( ',', '.', 2 )},
                {"data": 'account_remain',render: $.fn.dataTable.render.number( ',', '.', 2 )},
                {"data": null ,orderable: false, searchable: false}
            ],
            order: [[ 0, "ASC" ]],
            columnDefs: [
                    // { "orderable": false, "targets": 13},
                    {className: "text-center", "targets": [0, 1, 2, 3, 4, 8]},
                    {className: "text-right", "targets": [5,6,7]},
                    {
                        "render": function (data, type, row, meta) {
                            if(data.details_url != undefined){
                                dataReturn = '<a href="' + data.details_url + '" class="label label-primary">' + data.username + '</a>';
                            }else{
                                dataReturn = data.username;
                            }
                            return dataReturn;
                        },
                        "targets": 0
                    },
                    {
                        "render": function (data, type, row, meta) {
                            if(data.locked == 0)
                            {
                                dataReturn = '<span class="label label-flat text-success-600"><i class="icon-unlocked2"></i></span>';
                            }
                            else
                            {
                                dataReturn = '<span class="label label-flat text-danger-600"><i class="icon-lock5"></i></span>';
                            }
                            return dataReturn;
                        },
                        "targets": 1
                    },
                    {
                        "render": function (data, type, row, meta) {
                            dataReturn = '<span class="label label-flat border-primary text-primary-600">'+ data.roles_name+'</span>';
                            return dataReturn;
                        },
                        "targets": 4
                    },
                    {
                        "render": function (data, type, row, meta) {
                            dataReturn = '';
                            dataReturn += '<ul class="icons-list">';
                            dataReturn += '	<li class="dropdown">';
                            dataReturn += '		<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
                            dataReturn += '			<i class="icon-menu9"></i>';
                            dataReturn += '		</a>';
                            // dataReturn += '		<ul class="dropdown-menu dropdown-menu-right">';
                            // dataReturn += '			<li><a href="javascript:" onclick="deleteRecord(' + data.id + ')" rel=""><i class="icon-bin"></i>ลบ</a></li>';
                            // dataReturn += '		</ul>';
                            dataReturn += '	</li>';
                            dataReturn += '</ul>';

                            return dataReturn;
                        },
                        "targets": 8
                    },
                ],
            });

    });

    function deleteRecord(id) {
            swal({
                title: "Confirm Delete",
                text: "Are you sure you want to delete this Product ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                $.ajax({
                    url : "{{ url('backend/users')}}" + '/' + id,
                    type : "POST",
                    data : {'_method' : 'DELETE'},
                    success: function () {
                        swal({title: "Success", text: "Product has been deleted! \nClick 'Ok' to refresh page.", type: "success"},
                            function(){
                                location.reload();
                            }
                        );
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error!", "Please try again", "error");
                    }
                });
            });
    }
</script>

@endsection
